---
marp: true
headingDivider: 3
paginate: true
---

# `Git` トレーニング <!-- omit in toc -->

## 目次 <!-- omit in toc -->

- [1. 事前準備](#1-事前準備)
- [2. ゴール](#2-ゴール)
- [3. Git（GitLab） 説明](#3-gitgitlab-説明)
- [4. まとめ](#4-まとめ)
- [5. Appendix](#5-appendix)

## 1. 事前準備

1. `Sourcetree`をインストール
2. `GitLab`へのログインできることを確認

## 2. ゴール

`Git（およびGitLab）`を利用した基本的な開発の流れを理解する。

## 3. Git（GitLab） 説明

### リポジトリとは？ <!-- omit in toc -->

- `ファイル`や`ファイルの変更履歴`を
  保存しておく場所。
- `リモートリポジトリ`と`ローカルリポジトリ`の 2 種類ある。

![bg right w:550](images/リポジトリ.drawio.png)

### ハンズオン：クローン <!-- omit in toc -->

`リモートリポジトリ`を`ローカルリポジトリ`に複製。

### ブランチとは？ <!-- omit in toc -->

複数作業を並行して行う際に、
作業履歴を分岐させる機能。

- `develop`ブランチ
  - 開発中のソース置き場
  - 全員のソースが集まる
  - ここのソースを単体試験
- `feature`ブランチ
  - 機能追加変更を行う場所
  - 個人専用

![bg right w:550](images/ブランチ.drawio.svg)

### ハンズオン：`featureブランチ`の作成とコミット <!-- omit in toc -->

1. GitFlow 初期化（初回のみ）
2. `featureブランチ`の作成
3. 機能追加（コーディング）
4. ステージング → コミット
5. コミット修正（おまけ）
   1. 直前
   2. 2 個以上前
6. ブランチ切り替え（おまけ）

### プッシュとは？ <!-- omit in toc -->

`ローカル`リポジトリのブランチを
`リモート`リポジトリへアップロード。

![bg right w:550](images/プッシュ.drawio.svg)

### ハンズオン：プッシュ <!-- omit in toc -->

1. プッシュ
2. プッシュされたことを確認
   1. GitLab
   2. Sourcetree

### マージリクエスト（MR）とは？ <!-- omit in toc -->

2 つの意味がある。

1. ブランチを他のブランチへマージ**依頼**
   例：元`feature1` → 先`develop`
2. レビュー依頼

![bg right w:550](images/マージリクエスト.drawio.svg)

### ハンズオン：マージリクエスト（MR） <!-- omit in toc -->

1. マージリクエスト（＝レビュー依頼）の作成
2. Teams などでレビュー依頼
3. 他の人がコードレビュー

### マージとは？ <!-- omit in toc -->

2 つの意味がある。

1. ブランチを他のブランチへのマージ
   例：元`feature1` → 先`develop`
2. レビュー OK

※ 設定によっては元ブランチが削除されます。

![bg right w:550](images/マージ.drawio.svg)

### ハンズオン：マージ <!-- omit in toc -->

1. マージ

### プルとは？ <!-- omit in toc -->

`リモート`リポジトリのブランチを
`ローカル`リポジトリへダウンロード。

![bg right w:550](images/プル.drawio.svg)

### ハンズオン：プル <!-- omit in toc -->

1. フェッチ
2. プル

## 4. まとめ

`Git（およびGitLab）`を利用した
基本的な開発の流れ。

![bg right w:600](images/まとめ.drawio.svg)

## 5. Appendix

### 5.1. リリース <!-- omit in toc -->

<div class="mermaid" align="center">
gitGraph
   commit id: " " tag: "Ver.1"
   branch develop order: 2
   commit id: "機能1"
   commit id: "機能2"
   commit id: "機能3"
   branch release order: 1
   commit id: "微修正1"
   commit id: "微修正2"
   checkout main
   merge release tag: "Ver.2"
   checkout develop
   merge release
</div>

### 5.2. 障害対応 <!-- omit in toc -->

<div class="mermaid" align="center">
gitGraph
   commit id: " " tag: "Ver.1"
   branch develop order: 2
   commit id: "機能1"
   commit id: "機能2"
   commit id: "機能3"
   checkout main
   branch hotfix order: 1
   commit id: "バグ修正1"
   checkout main
   merge hotfix tag: "Ver.1.1"
   checkout develop
   merge hotfix
</div>
